import matplotlib.pyplot as plt
import numpy as np
import copy, imageio, os
import matplotlib.colors
from collections import Counter
from tqdm import tqdm
import scipy.ndimage as sc


def visualization1(N, p, string):
    my_cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ["whitesmoke","green","red", "black"])

    images = []
    scale_factor = 10
    scaling_matrix = np.ones((scale_factor, scale_factor))

    for i, matrix in enumerate(forest_generator(N, p, get_all = True)):
        scaled_matrix = np.kron(matrix, scaling_matrix)
        plt.imsave('burning_%d.png' %i, scaled_matrix, cmap=my_cmap, vmin=0, vmax=3)
        images.append(imageio.imread('burning_%d.png' %i))
    imageio.mimsave("forest_%s.gif"%string, images, duration=0.05)
    for j in range(i+1):
        os.remove('burning_%d.png' %j)    

def forest_generator(N, p, get_all=False):
    nums = np.random.choice([0, 1], size=(N, N), p=[1-p, p])
    matrices = [nums]
    nums[-1, :] = np.where(nums[-1, :] != 1, nums[-1, :], 2)
  
    zeros = np.zeros((N+2, N+2))
    zeros[1:N+1, 1:N+1] = nums
    nums = zeros
    
    nums_2 = copy.deepcopy(nums)
    while 2 in nums:
        if get_all:
            yield nums[1:N+1, 1:N+1]
        for row, column in np.argwhere(nums_2 == 2):
            auxiliary = nums_2[row-1:row+2, column-1:column+2]
            auxiliary[auxiliary == 1] = 2
            auxiliary[1,1] = 3
            nums[row-1:row+2, column-1:column+2] = auxiliary
    matrices.append(nums[1:N+1, 1:N+1])
    yield matrices[-1]
    
def get_p0(x,y):
    return x[np.argmax(np.diff(y))]

def get_p(N, p, MC = 300):
    prob = 0
    for _ in range(MC):
        forest_result = next(forest_generator(N, p, get_all = False))
        if sum(forest_result[0,:] == 3) > 0:
            prob += 1    
    return prob/MC

def q_plot(N):
    q = []
    probs = np.linspace(0,1,51)
    for p in tqdm(probs):
        q.append(get_p(N, p))
    plt.plot(probs, q, '.')
    plt.title('p*(p) for L = %i and 300 Monte Carlo steps'%N)
    plt.xlabel('p')
    plt.ylabel('p*')
    plt.savefig("p_plot_%d"%N)
    plt.close()
    
    

    
N = 20
p = 0.4
L = [20, 50, 100]
MC = 50

p_list = np.linspace(0,1,51)



# save plot for first and last step of simulation:
# my_cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ["whitesmoke","green","red", "black"])
# scale_factor = 10
# scaling_matrix = np.ones((scale_factor, scale_factor))
# for i, matrix in enumerate(forest_generator(N, p, get_all=True)):
#     if i == 0:
#         scaled_matrix = np.kron(matrix, scaling_matrix)
#         plt.imsave('first.png', scaled_matrix, cmap=my_cmap, vmin=0, vmax=3)
# scaled_matrix = np.kron(matrix, scaling_matrix)
# plt.imsave('last.png', scaled_matrix, cmap=my_cmap, vmin=0, vmax=3)

# Percepolation plots:
# for N in L:
#     q_plot(N)

# Create gifs (simulation):
# for N in L:
#     visualization1(N, p, str(N))

# Create plots with biggest cluster
# biggest_clusters = []
# y = []
# for N in L:    
#     y = []
#     for p in tqdm(p_list):
#         biggest_clusters = []
#         for _ in range(MC):
#             matrix = next(forest_generator(N, p))
#             labeled, maks = sc.label(matrix)
#             unique, counts = np.unique(labeled, return_counts=True)
#             if np.all(matrix == 0):
#                 biggest_clusters = [0]
#             elif np.all(matrix == 3):
#                 biggest_clusters = [matrix.size]
#             else:
#                 clusters_counter = dict(zip(unique, counts))
#                 del clusters_counter[0]
#                 biggest = max(clusters_counter.values())
#                 biggest_clusters.append(biggest)
#         y.append(np.mean(biggest_clusters))
#     plt.plot(p_list, y, '.')
#     plt.xlabel("p")
#     plt.ylabel("max cluster")
#     plt.title("Function of the biggest cluster for N = "+str(N))
#     plt.savefig("p_plot_%d"%N)
#     plt.close()


